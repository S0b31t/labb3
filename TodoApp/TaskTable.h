//
//  TaskTable.h
//  TodoApp
//
//  Created by Johan H on 2015-02-08.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskTable : UITableViewController

-(IBAction) editButtonClicked:(id)sender;

@end

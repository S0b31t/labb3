//
//  AddTaskViewController.m
//  TodoApp
//
//  Created by Johan H on 2015-02-04.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import "AddTaskViewController.h"

@interface AddTaskViewController ()

@property (weak, nonatomic) IBOutlet UITextField *addTask;


@end

@implementation AddTaskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addTaskButton:(id)sender {
    
    [self.tasks addObject:self.addTask.text];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
